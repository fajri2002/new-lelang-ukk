<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\Barang;
class BarangController extends Controller
{
    public function index()
    {
        $barang = Barang::all();
        return view('barang.index',compact('barang'));
    }

   

    public function create()
    {
        return view('barang.form');
    }

    public function store(Request $request)
    {
        $rules = $request->validate ([
            'nama_barang' => 'required',
            'deskripsi_barang' => 'required',
            'harga_awal' => 'required',
        ]);
        $status = Barang::create([
            'user_id' => Auth::id(),
            'nama_barang' => $request ->nama_barang,
            'deskripsi_barang' => $request ->deskripsi_barang,
            'harga_awal' => $request ->harga_awal,

        ]);

        if ($status)return redirect('barang')->with('success', 'Data berhasil ditambahkan');
        else return redirect()->back()->with('error', 'Data gagal ditambahkan');
    }

    public function edit($id)
    {
        $data['result'] = \App\Models\Barang::where('id', $id)->first();
        return view('barang.form')->with($data);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'user_id' =>'required',
            'nama_barang' => 'required',
            'deskripsi_barang' => 'required',
            'harga_awal' => 'required',
        ];
        $this->validate($request, $rules);
        $input = $request->all();
        $result = Barang::where('id', $id)->first();
        $status = $result->update($input);

        if ($status) return redirect('/')->with('success', 'Data berhasil diubah');
        else return redirect('/tambah')->with('error', 'Data gagal diubah');

    }

    public function destroy(Request $request, $id)
    {
        $result = \App\Models\Barang::where('id', $id)->first();
        $status =  $result->delete();

        if ($status) return redirect('/')->with('success', 'Data berhasil dihapus');
        else return redirect('barang')->with('error', 'Data gagal dihapus');

    }

}
