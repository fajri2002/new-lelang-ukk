@extends('layouts.header')

@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Input Barang</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
              <li class="breadcrumb-item active"> Input Barang</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <!-- Main content -->
  <section class="content">
  @if(session('success'))
    <div class ="alert alert-success fade in">
        <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times</span>
        </button>
        {!! session('success')}
    </div>
@endif

@if(session('success'))
    <div class ="alert alert-success fade in">
        <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times</span>
        </button>
        {!! session('success')}
    </div>
@endif
      <div class="box">
          <div class = "box-header with-border">
              <a href="{{ route('barang.add') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i>
              Tambah </a>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Barang</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Barang</th>
                  <th>Deskripsi</th>
                  <th>Harga Awal</th>
                </tr>
                </thead>
                <tbody>
                @foreach($barang as $data)
                <tr>
                  <td>{{!empty($i) ? ++$i : $i = 1}}</td>
                  <td>{{$data->nama_barang}}</td>
                  <td>{{$data->deskripsi_barang}}</td>
                  <td>{{$data->harga_awal}}</td>
                </tr>
                @endforeach
                </tbody>
              </table>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  @endsection