@extends('layouts.header')

@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Data Barang</h1>
            <small>Lelang Online</small>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('barang.index')}}">Home</a></li>
              <li class="breadcrumb-item active">Data Barang</li>
              <li class="active">Tambah Data Barang</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
    @if(session('success'))
    <div class ="alert alert-success fade in">
        <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times</span>
        </button>
        {!! session('success')}
    </div>
@endif

@if(session('error'))
    <div class ="alert alert-danger fade in">
        <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times</span>
        </button>
        {!! session('error')}
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif 

    <div class="box">
        <div class="box-header with-border">
            <a href="{{ route('barang.index') }}" class="btn bg-blue"><i class="fa fa-chevron-left"></i>Kembali
             </a>
        </div>
        <div class="box-body">
        <form action="{{ route('barang.add.submit')}}" class="form-horizontal" method="POST">
            {{csrf_field()}}

            <div class="form-group">
                <label class="conrol-label col-sm-2">Nama Barang</label>
                <div class="col-sm-10">
                    <input type="text" name="nama_barang" class="form-control" placeholder="Masukkan nama barang"
                    />
                </div>
            </div>

            <div class="form-group">
                <label class="conrol-label col-sm-2">Deskripsi</label>
                <div class="col-sm-10">
                    <input type="text" name="deskripsi_barang" class="form-control" placeholder="Masukkan deskripsi barang"
                    />
                </div>
            </div>

            <div class="form-group">
                <label class="conrol-label col-sm-2">Harga Awal</label>
                <div class="col-sm-10">
                    <input type="text" name="harga_awal" class="form-control" placeholder="Masukkan harga awal"
                    />
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan
                    </button>
                    </div>
                </div>
             </form>
            </div>
        </div>
     </section>
@endsection

    
